from bot_yuv.chat_functions import send_text_to_room
from bot_yuv.mycaldav import nextevents


class Command(object):
    def __init__(self, client, store, config, command, room, event):
        """A command made by a user

        Args:
            client (nio.AsyncClient): The client to communicate to matrix with

            store (Storage): Bot storage

            config (Config): Bot configuration parameters

            command (str): The command and arguments

            room (nio.rooms.MatrixRoom): The room the command was sent in

            event (nio.events.room_events.RoomMessageText): The event describing the command
        """
        self.client = client
        self.store = store
        self.config = config
        self.command = command
        self.room = room
        self.event = event
        self.args = self.command.split()[1:]

    async def process(self):
        """Process the command"""
        if self.event.sender not in self.config.trusted_users:
            await self._untrusted()
            return
        if self.command.startswith("echo"):
            await self._echo()
        elif self.command.startswith("agenda"):
            await self._agenda()
        elif self.command.startswith("help"):
            await self._show_help()
        elif self.command.startswith("test"):
            await self._test()
        else:
            await self._unknown_command()

    async def _untrusted(self):
        """Reply to unknown user"""
        response = "Who do you think you are"
        await send_text_to_room(self.client, self.room.room_id, response)

    async def _echo(self):
        """Echo back the command's arguments"""
        response = " ".join(self.args)
        await send_text_to_room(self.client, self.room.room_id, response)

    async def _agenda(self):
        """show agenda from caldav"""
        if not self.args:
            events_sum = nextevents (self.config.caldav_url, self.config.caldav_user, self.config.caldav_password)
            response = ""

            for event in events_sum:
                response = response + "\n* " + event[1][0] + " " + event[1][1] + " " + event[1][2]

            await send_text_to_room(self.client, self.room.room_id, response, markdown_convert=True)
            return

        topic = self.args[0]
        if topic == "":
            text = "no topic"
        else:
            events_sum = nextevents (self.config.caldav_url, self.config.caldav_user, self.config.caldav_password, topic)
            response = ""
            for event in events_sum:
                response = response + "\n* " + event[1][0] + " " + event[1][1] + " " + event[1][2]
                
            await send_text_to_room(self.client, self.room.room_id, response)
            return

        await send_text_to_room(self.client, self.room.room_id, text)
        return    

    async def _show_help(self):
        """Show the help text"""
        if not self.args:
            text = (
                "Hello, I am a bot-yuv made with matrix-nio! Use `help commands` to view "
                "available commands."
            )
            await send_text_to_room(self.client, self.room.room_id, text,"bis")
            return

        topic = self.args[0]
        if topic == "rules":
            text = "These are the rules!"
        elif topic == "commands":
            text = "Available commands: agenda, echo, debug"
        elif topic == "agenda":
            text = "`agenda` to show all, `agenda [topic]` to filter"
        else:
            text = "Unknown help topic!"
        await send_text_to_room(self.client, self.room.room_id, text)
    async def _test(self):
        """test command's arguments"""
        str1 = "premier"
        response = "* " + str1 + "\n* " + str1
        await send_text_to_room(self.client, self.room.room_id, response, markdown_convert=True)
    
    async def _unknown_command(self):
        await send_text_to_room(
            self.client,
            self.room.room_id,
            f"Unknown command '{self.command}'. Try the 'help' command for more information.",
        )
