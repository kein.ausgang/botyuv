import caldav
import datetime
import pical
import pytz


class Calendars:
    def __init__(self, davurl, login, pwd):

        self.davurl = davurl
        self.login = login
        self.pwd = pwd
        client = caldav.DAVClient(url=self.davurl, username=self.login, password=self.pwd)  
        self.calendar = caldav.Calendar(client=client, url=self.davurl)

    def getevents(self, days=31, filter=''):
        '''return events in the next DAYS'''
        self.days = days
        self.filter = filter
        start = datetime.datetime.now(datetime.timezone.utc)
        end = start + datetime.timedelta(days=self.days)
        future_events = self.calendar.date_search(start=start, end=end, expand=True)
        
        all_events = {}
        i = 0
        for ics in range(len(future_events)):
            cal = pical.parse(future_events[ics].data.splitlines())[0]
            for c in cal.children:
                 if c.name == "VEVENT":
                     if self.filter.upper() in c["SUMMARY"].upper():
                         dtstart = c["DTSTART"]
                         if isinstance (dtstart, datetime.datetime):
                             dtstart = datetime.datetime.date(dtstart)                        
                         if datetime.datetime.date(start) <= dtstart <= datetime.datetime.date(end):
                            all_events[i] = c
                            i += 1
        return all_events



    def summary(self,all_events):
        '''return only SUMMARY and DTSTART from events'''
        self.all_events = all_events
        self.all_sum = {}

        for i in self.all_events:
            try:
                e_title = self.all_events[i]['SUMMARY']
            except:
                e_title = 'title unknown'


            e_start = self.all_events[i]['DTSTART']
            try:
                e_start = self.localtime(e_start)
            except:
                pass
            fmt = '%d-%m-%Y - %H:%M:%S'
            e_start = e_start.strftime(fmt)

            try:
                e_location = self.all_events[i]['LOCATION']
            except:
                e_location = ''
                      
            self.all_sum[i] = e_title, e_start, e_location
       
        self.all_sum = sorted(self.all_sum.items(), key = lambda x: datetime.datetime.strptime(x[1][1], "%d-%m-%Y - %H:%M:%S"))
        return self.all_sum

    def localtime(self,time_obj,time_zone='Europe/Brussels'):
        '''get localtime from utc time'''
        localtc = pytz.timezone(time_zone)
        time_local = localtc.normalize(time_obj.astimezone(localtc))
        return time_local


def nextevents (cal_url, cal_login, cal_password, cal_filter=''):
    #try:
        mycal = Calendars (cal_url, cal_login, cal_password)
        events = mycal.getevents(7, cal_filter)
        events_sum = mycal.summary(events)
        return(events_sum)
    #except:
    #    msg = "something went wrong"
    #    return(msg)

if __name__ == "__main__":
    print("main")
